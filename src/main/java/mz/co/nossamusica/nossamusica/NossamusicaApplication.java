package mz.co.nossamusica.nossamusica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NossamusicaApplication {

    public static void main(String[] args) {
        SpringApplication.run(NossamusicaApplication.class, args);
    }

}
